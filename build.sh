docker build \
  --no-cache  \
  --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') \
  -t registry.gitlab.com/${PROJECT}/cowsay:v2.0.0 \
  -f dockerfiles/Dockerfile context

